## DESCRIPTION 
Program: investment_report
Author:  Dominika Egeman

## ANALYSIS: 
This program aims to computes an investment report

## Significant constants:

## The inputs are:
- initial_invested_amount 
- duration
- interest_rate

## The output is:
- report (tabular form)


## Computations:
- for each year compute the interest and add it to the investment 
- print a formatted row of results for that year
- ending investment and interest  earned are also displayed

## REASONABLE SET OF LEGITIMATE INPUTS:

- negative numbers - check if number cannot be negative to proceed.
