import pandas


class FinalReport():
    __df = pandas.DataFrame()
    
    def __init__(self):
        self.years = []
        self.starting_balance = []
        self.interests = []
        self.ending_balance = []
        self.final_ending_balance = []
        self.total_interest_earned = []
    
    def dframe(self):
        # self.__df.to_csv()
        self.__df.to_csv("./investment_report/final_report.csv")

        data = {
            "Year": self.years,
            "Starting balance": self.starting_balance,
            "Interest": self.interests,
            "Ending balance": self.ending_balance,
        }

        null_rows = []
        for item in range(len(self.years)):
            empty_space = ""
            null_rows.append(empty_space)
        
        
        
        __df_new_data = pandas.DataFrame(data, index=null_rows)
        __df_new_data.to_csv("./investment_report/final_report.csv", mode="a")
        
        self.final_ending_balance = self.ending_balance[-1]
        self.total_interest_earned = sum(self.interests)   
        
        
        
        df2 = pandas.DataFrame({"Year": ["%0.1f" % self.final_ending_balance]},index=["Final Ending Balance"])
        df3 = pandas.DataFrame({"Year": ["%0.1f" % self.total_interest_earned]},index=["Total Interest Earned"])

        df_final = pandas.concat([__df_new_data, df2, df3])
        

        df_final["Year"].fillna("",inplace=True)
        df_final["Starting balance"].fillna("",inplace=True)
        df_final["Interest"].fillna("", inplace=True)
        df_final["Ending balance"].fillna("", inplace=True)
    
        print(f"\n{df_final}")
        
    
    
    def prompt_input(self):
        
        self.investment_amount = int(input("Enter the investment amount: "))
        self.duration = int(input("Enter the number of years: "))
        self.rate = int(input("Enter the rate as a %: "))
    
    def calculation(self):
        percentage = self.rate / 100
        whole_duration_period = self.duration + 1
        
        for year in range(1, whole_duration_period):
            
            if year == 1:
                self.starting_balance.append(self.investment_amount)
            elif year != 1:
                self.starting_balance.append(self.ending_balance[-1])    
                    
            self.years.append(year)
            self.current_interest = self.starting_balance[-1] * percentage
            self.interests.append(self.current_interest)
            new_starting_balance = self.starting_balance[-1] + self.current_interest
            self.ending_balance.append(new_starting_balance)
            


    
        
        


