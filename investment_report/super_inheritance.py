import pandas



class FinalReport(pandas.DataFrame):
    #__df = pandas.DataFrame()
    def __init__(self):
        super().__init__()
        self.investment_amount = 0
        self.duration = 0
        self.rate = 0
    
    def df(self):
        # self.__df.to_csv()
        data_frame = pandas.DataFrame({}).to_csv("./investment_report/final_report.csv")
        years = []
        starting_balance = []
        interests = []
        ending_balance = []
        final_ending_balance = 0
        total_interest_earned = 0

        data = {
            "Year": years,
            "Starting balance": starting_balance,
            "Interest": interests,
            "Ending balance": ending_balance,
            "Final ending balance": final_ending_balance,
            "Total interest earned": total_interest_earned
        }
            
        # self.data = {
        #     "Year": self.years,
        #     "Starting balance":self.starting_balance,
        #     "Interest": self.interests,
        #     "Ending balance": self.ending_balance,
        #     "Final ending balance": self.final_ending_balance,
        #     "Total interest earned": self.total_interest_earned
        #}

        
        data_frame = pandas.DataFrame(data, index=[0])
        data_frame.to_csv("./investment_report/final_report.csv", mode="a")

    
    def prompt_input(self):
        self.investment_amount = int(input("Enter the investment amount: "))
        self.duration = int(input("Enter the number of years: "))
        self.rate = int(input("Enter the rate as a %: "))
    
    def calculate(self):
        pass


run = FinalReport()
run.df()