from run_final_report import FinalReport

if __name__ == "__main__":
    run = FinalReport()
    run.prompt_input()
    run.calculation()
    run.dframe()
